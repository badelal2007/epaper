#!/bin/bash
#Author Bade Lal
echo -ne '\033c'

while true; do
    echo "Enter date to download epaper, Accepted date format is YYMMDD.";
    echo "Press y/Y to download today's epaper.";
    echo "Press E to exit.";

    read -p "Enter your option : " dt

    if [ $dt = "e" ] || [ $dt = "E" ]
      then
        flag='e'
    elif [ $dt = "y" ] || [ $dt = "Y" ]
      then
        flag=1
    elif [[ $dt =~ ^[0-9]{2}[0-9]{2}[0-9]{2}$ ]] && date -d "$dt" >/dev/null
      then
        #echo "Date $dt is valid and matches the format (YYMMDD)"
        flag=2
    else
        flag=0
    fi

    case $flag in
        1 )
            #echo "Option 1"
            df1=$(date '+%Y/%m/%d')
            df2=$(date '+%d_%m_%Y')
            df3=$(date '+%d-%B-%Y')
            #echo $dt" "$flag;
            #wget -O $HOME/Desktop/TimesOfIndia-$(date '+%d_%m_%Y').pdf $(echo "http://epaperbeta.timesofindia.com/NasData//PUBLICATIONS/THETIMESOFINDIA/DELHI/")$(date '+%Y/%m/%d/PagePrint/%d_%m_%Y_')$(echo -n $(date '+%d_%m_%Y_pressguess') | md5sum | awk '{print $1}')$(echo ".pdf")
            break;;
        2 )
            #echo "Option 2"
            df1=$(date -d $dt +'%Y/%m/%d')
            df2=$(date -d $dt +'%d_%m_%Y')
            df3=$(date -d $dt +'%d-%B-%Y')
            #echo "Please wait while downloading ...$df2"
            #wget -O $HOME/Desktop/TimesOfIndia-$df2.pdf $(echo "http://epaperbeta.timesofindia.com/NasData//PUBLICATIONS/THETIMESOFINDIA/DELHI/$df1/PagePrint/$df2"_)$(echo -n $df2'_pressguess' | md5sum | awk '{print $1}')$(echo ".pdf")
            break;;
        [eE] ) echo "You exited successfully"; exit;;
        * )
        #echo -ne '\033c'
        echo "You have entered invalid date format. Your entered date is : [$dt]";;
    esac
done

PUBLICATIONS=([0]='The Times Of India' [1]='The Economic Times' [2]='Mirror')

#echo "${!PUBLICATIONS[@]} | tr ' ' '|'"

echo "****************************Publications are**************************"
for p in "${!PUBLICATIONS[@]}"
do
  echo "$p.) ${PUBLICATIONS[$p]}"
done
echo "**********************************************************************"

pub_size=$((${#PUBLICATIONS[@]}-1))
while true; do
    read -p "Enter publication you wish to select [0-${pub_size}]: " publ

    if [[ " ${!PUBLICATIONS[@]} " =~ " ${publ} " ]]; then
      e_publ="${PUBLICATIONS[$publ]}"
      p_found="FOUND"
    else
      p_found="NOT_FOUND"
    fi

    case $p_found in
      'FOUND' )
        echo "**********************************************************************"
        echo "Selected Publication: ${PUBLICATIONS[$publ]}";
        echo "**********************************************************************"
        echo ""
        #echo "Thanks."
        break;;
        * ) echo "Please select the correct numeric serial.";;
    esac
done

TOI=([0]='AHMEDABAD' [1]='BANGALORE' [2]='CHANDIGARH' [3]='CHENNAI' [4]='DELHI' [5]='HYDERABAD' [6]='JAIPUR' [7]='KOCHI' [8]='KOLKATA' [9]='LUCKNOW' [10]='MUMBAI' [11]='NAVIMUMBAI' [12]='PUNE' [13]='THANE')
ECONOMY=([0]='BANGALORE' [1]='DELHI' [2]='KOLKATA' [3]='MUMBAI')
MIRROR=([0]='AHMEDABAD' [1]='BANGALORE' [2]='MUMBAI' [3]='PUNE')

if [ $publ = 0 ]
then
    e_publ="THETIMESOFINDIA"
    editions=("${TOI[@]}")
elif [ $publ = 1 ]
then
    e_publ="THEECONOMICTIMES"
    editions=("${ECONOMY[@]}")
elif [ $publ = 2 ]
then
    e_publ="MIRROR"
    editions=("${MIRROR[@]}")
else
    editions="NOT_FOUND"
fi


arr_size=$((${#editions[@]}-1))

echo "${PUBLICATIONS[$publ]} epaper editions are"
echo "**********************************************************************"
for i in "${!editions[@]}"
do
  echo "$i.) ${editions[$i]}"
done
echo "**********************************************************************"

while true; do
    echo ""
    read -p "Enter edition you wish to select [0-${arr_size}]: " ed
    case $ed in
       [0-9]|10|1[1-3] )
        echo "..................................."
        echo "....Downloading is in progress....."
        echo "..................................."
        e_edition="${editions[$ed]}"
        wget -O $HOME/Desktop/$e_publ-$df3.pdf $(echo "http://epaperbeta.timesofindia.com/NasData//PUBLICATIONS/$e_publ/$e_edition/$df1/PagePrint/$df2"_)$(echo -n $df2'_pressguess' | md5sum | awk '{print $1}')$(echo ".pdf")

        # echo "Selected date is ${df1} and ${df2}"
        # echo "Selected Publication is ${e_publ}"
        # echo "Selected Edition is ${editions[$ed]}"
        # echo "Thanks."
        break;;
        * ) echo "Please select the correct numeric serial.";;
    esac
done

# HEIGHT=15
# WIDTH=40
# CHOICE_HEIGHT=4
# BACKTITLE="Backtitle here"
# TITLE="Title here"
# MENU="Choose one of the following options:"

# OPTIONS=(1 "Option 1"
#          2 "Option 2"
#          3 "Option 3")

# CHOICE=$(dialog --clear \
#                 --backtitle "$BACKTITLE" \
#                 --title "$TITLE" \
#                 --menu "$MENU" \
#                 $HEIGHT $WIDTH $CHOICE_HEIGHT \
#                 "${OPTIONS[@]}" \
#                 2>&1 >/dev/tty)

# clear
# case $CHOICE in
#         1)
#             echo "You chose Option 1"
#             ;;
#         2)
#             echo "You chose Option 2"
#             ;;
#         3)
#             echo "You chose Option 3"
#             ;;
# esac
